package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



/**
 * @author aparnayadlapati
 *
 */
public class Login {
	WebDriver driver;
	By Email=By.id("user_email");
	By Password=By.id("user_password");
	By signin=By.className("btn btn-primary");

	public Login(WebDriver driver) {
		this.driver=driver;
	}
	
	public void setUserName(String strUserName){

        driver.findElement(Email).sendKeys(strUserName);

    }
	public void setPassword(String strPassword){

        driver.findElement(Password).sendKeys(strPassword);

   }
	//click on sign in button
	public void clickLogin(){

        driver.findElement(signin).click();

}
	
	/**
	 

     * This POM method will be exposed in test case to login in the application

     * @param strUserName

     * @param strPasword

     * @return

     */
	public void loginTotasks(String strUserName,String strPasword){

        //Fill user name

        this.setUserName(strUserName);

        //Fill password

        this.setPassword(strPasword);

        //Click Login button

        this.clickLogin();      
	}

	/**
	 * @param args
	 */
	

}
